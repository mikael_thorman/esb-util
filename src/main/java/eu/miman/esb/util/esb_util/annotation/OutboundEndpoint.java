package eu.miman.esb.util.esb_util.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This annotation is used to annotate child classes to OutboundRoute.
 * 
 * It ensures that the child class is setup in the correct way and that no configuration is missed.
 * 
 * @author Mikael Thorman
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface OutboundEndpoint {

	/**
	 * The channel where the message is sent (a queue or topic name)
	 * @return
	 */
	String channel();
	/**
	 * The Camel type of channel where the message shall be sent (ex jms or http).
	 * @return
	 */
	String channelType();
	/**
	 * Extra parameters that is needed for the URI, these are appended at the end of the URI in the correct way.
	 * @return
	 */
	String[] parameters() default "";
	/**
	 * The name of the route, if this is not set, the simple class name will be used.
	 */
	String routeName() default "";

}
