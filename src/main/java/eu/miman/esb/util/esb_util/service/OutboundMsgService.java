package eu.miman.esb.util.esb_util.service;

import org.apache.camel.Exchange;

import eu.miman.esb.util.esb_util.OutboundMessage;

/**
 * This service is used to set the correct transport headers from the received
 * OutboundMessage into the Exchange and also to set the actual payload to the
 * objectToSend.
 * 
 * @author Mikael Thorman
 */
public interface OutboundMsgService {

	/**
	 * Sets the appropriate headers based on the received object. Sets the body
	 * of the Exchange to the object to send from the received msg.
	 * 
	 * @param body The msg to send
	 * @param exchange The Camel Exchange
	 */
	public abstract void handle(OutboundMessage body, Exchange exchange);

}