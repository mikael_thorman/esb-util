package eu.miman.esb.util.esb_util;

import java.util.HashMap;
import java.util.Map;

import eu.miman.esb.util.esb_msg.EsbMessageBase;

/**
 * This is the transfer object for a message being sent to the MiMan ESB using the OutboundRouter.
 * 
 * The purpose of using these classes to post messages on the bus is to make sure that all necessary headers are there and that they are named consistently.
 * 
 * Also to make sure the correct message format is used.
 * 
 * This class is done so that you cannot create a message without setting the necessary parameters. 
 * 
 * @author Mikael Thorman
 */
public class OutboundMessage {
	/**
	 * The payload object to send (this will be converted to the message format used by the bus).
	 */
	EsbMessageBase objToSend;
	/**
	 * These are optional headers that can be added to the message, these will be set as property headers in the actual channel.
	 */
	Map<String, Object> headerMap;
	/**
	 * The unique message id for the message
	 */
	String msgId;
	/**
	 * The transaction id for the transaction this message transfer is a part of.
	 * 
	 * If this is set the receiver is assumed to set the same in the actual answer
	 */
	String transactionId;
	
	public OutboundMessage(EsbMessageBase objToSend, String msgid, String transactionId) {
		super();
		this.headerMap = new HashMap<String, Object>();
		this.objToSend = objToSend;
		this.msgId = msgid;
		this.transactionId = transactionId;
	}
	
	public OutboundMessage(EsbMessageBase objToSend, String msgid) {
		super();
		this.headerMap = new HashMap<String, Object>();
		this.objToSend = objToSend;
		this.msgId = msgid;
	}
	
	public EsbMessageBase getObjToSend() {
		return objToSend;
	}
	public Map<String, Object> getHeaderMap() {
		return headerMap;
	}
	public String getMsgId() {
		return msgId;
	}
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
}
