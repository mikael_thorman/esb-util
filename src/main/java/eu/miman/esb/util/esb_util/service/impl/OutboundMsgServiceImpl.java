package eu.miman.esb.util.esb_util.service.impl;

import java.util.Set;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.springframework.stereotype.Component;

import eu.miman.esb.util.esb_util.OutboundMessage;
import eu.miman.esb.util.esb_util.service.OutboundMsgService;

/* (non-Javadoc)
 * @see eu.miman.repo.user.routes.service.OutboundMsgService
 */
@Component
public class OutboundMsgServiceImpl implements OutboundMsgService {

	/* (non-Javadoc)
	 * @see eu.miman.repo.user.routes.service.OutboundMsgService#handle(eu.miman.repo.user.routes.OutboundMessage, org.apache.camel.Exchange)
	 */
	@Override
	public void handle(OutboundMessage body, Exchange exchange) {
		
		Message in = exchange.getIn();
		in.setBody(body.getObjToSend());
		
		in.setHeader("MessageType", body.getObjToSend().messageName());
		in.setHeader("MessageId", body.getMsgId());
		in.setHeader("MsgVersion", body.getObjToSend().messageVersion());	
		if (body.getTransactionId() != null) {
			in.setHeader("TransactionId", body.getTransactionId());	
		}
		
		Set<String> items = body.getHeaderMap().keySet();
		for (String key : items) {
			Object obj = body.getHeaderMap().get(key);
			in.setHeader(key, obj);
		}
		
		exchange.setOut(in);
	}
}
