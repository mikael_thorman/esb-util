package eu.miman.esb.util.esb_util;

/**
 * This interface is used as an interface towards the OutboundRoute, so that the correct input data is sent to this route.
 *   
 * @author Mikael Thorman
 */
public interface OutboundEndpoint {

	/**
	 * Sends the msg to the ESB on the correct queue with the correct headers and payload format.
	 * @param msg The message to send
	 */
	void sendMsg(OutboundMessage msg);
}
