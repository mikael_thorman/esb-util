package eu.miman.esb.util.esb_util;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import eu.miman.esb.util.esb_util.service.OutboundMsgService;
import eu.miman.esb.util.esb_util.service.impl.OutboundMsgServiceImpl;

/**
 * This is the Spring configuration class for this module.
 * 
 * Import the beans in this module by adding this annotation @Import(EsbUtilSpringConfig.class) to your @Configuration annotated application class.
 * 
 * @author Mikael Thorman
 */
@Configuration
public class EsbUtilSpringConfig {

	@Bean
	public OutboundMsgService outboundMsgService() {
		return new OutboundMsgServiceImpl();
	}
}
