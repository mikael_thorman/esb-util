package eu.miman.esb.util.esb_util.route;

import java.lang.annotation.Annotation;

import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.miman.esb.util.esb_util.annotation.OutboundEndpoint;
import eu.miman.esb.util.esb_util.service.OutboundMsgService;

/**
 * This is a Camel route that can be used by an internal Java class to send a message onto 
 * the ESB.
 * 
 * By sending an OutboundMessage object to this route it makes sure the message is 
 * sent in the correct way to the ESB (with headers and the right message format of the payload).
 * 
 * This Route must be overridden with a class annotated with the OutboundMessage annotation.
 *  
 * @author Mikael Thorman
 */
@Component
abstract public class OutboundRoute extends SpringRouteBuilder {
	@Autowired
	OutboundMsgService outboundMsgService;
	
	/**
	 * The actual route definition.
	 */
    @Override
    public void configure() throws Exception {
        from("direct:" + getRouteName())
        	.id(getRouteName())
        	.bean(outboundMsgService, "handle")
        	.marshal().json(JsonLibrary.Jackson)
        	.to(getToUri())
        	.log("Message sent: ${body}")
        	.end();
    }

    /**
     * Composes the correct Camel route URI based on the OutboundRoute annotations.
     * 
     * Result Example: jms:AHP.USER.MODIFIED.EVENT?jmsMessageType=Text&disableReplyTo=true
     * 
     * @return A Camel route URI based on the OutboundRoute annotations
     */
    protected String getToUri() {
    	if (this.getClass().isAnnotationPresent(OutboundEndpoint.class)) {
    		Annotation annotation = this.getClass().getAnnotation(OutboundEndpoint.class);
    		OutboundEndpoint outboundEndpoint = (OutboundEndpoint) annotation;
    		StringBuffer sb = new StringBuffer();
    		sb.append(outboundEndpoint.channelType()).append(":").append(outboundEndpoint.channel());
    		if (outboundEndpoint.parameters().length > 0) {
    			sb.append("?");
    		}
    		boolean firstElem = true;
    		for (String param : outboundEndpoint.parameters()) {
    			if (firstElem) {
    				firstElem = false;
    			} else {
    				sb.append("&");
    			}
    			sb.append(param);
			}
    		return sb.toString();
    	}
    	return null;
    }

    /**
     * Extracts the route name from the OutboundEndpoint annotations
     * @return The route name extracted from the OutboundEndpoint annotations
     */
    protected String getRouteName() {
    	if (this.getClass().isAnnotationPresent(OutboundEndpoint.class)) {
    		Annotation annotation = this.getClass().getAnnotation(OutboundEndpoint.class);
    		OutboundEndpoint outboundEndpoint = (OutboundEndpoint) annotation;
    		if (outboundEndpoint.routeName().length() == 0) {
    			return this.getClass().getSimpleName();
    		} else {
    			return outboundEndpoint.routeName();    			
    		}
    	}
    	return null;
    }
}

