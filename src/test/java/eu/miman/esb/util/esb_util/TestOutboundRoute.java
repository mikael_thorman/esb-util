package eu.miman.esb.util.esb_util;

import org.springframework.stereotype.Component;

import eu.miman.esb.util.esb_util.annotation.OutboundEndpoint;
import eu.miman.esb.util.esb_util.route.OutboundRoute;

@OutboundEndpoint(channel = "AHP.USER.MODIFIED.EVENT", channelType = "jms", routeName = "TestRoute", 
	parameters = {"jmsMessageType=Text", "disableReplyTo=true" })
@Component
public class TestOutboundRoute extends OutboundRoute {
}
