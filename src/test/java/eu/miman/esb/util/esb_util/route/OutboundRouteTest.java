package eu.miman.esb.util.esb_util.route;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.miman.esb.util.esb_util.TestOutboundRoute;
import eu.miman.esb.util.esb_util.TestOutboundWithNoNameRoute;

public class OutboundRouteTest {

	@Test
	public void testGetToUri() {
		TestOutboundRoute c2t = new TestOutboundRoute();
		String actual = c2t.getToUri();
		assertEquals("jms:AHP.USER.MODIFIED.EVENT?jmsMessageType=Text&disableReplyTo=true", actual);
		actual = c2t.getRouteName();
		assertEquals("TestRoute", actual);
	}

	@Test
	public void testGetToUriWithNoName() {
		TestOutboundWithNoNameRoute c2t = new TestOutboundWithNoNameRoute();
		String actual = c2t.getToUri();
		assertEquals("jms:AHP.USER.MODIFIED.EVENT?jmsMessageType=Text&disableReplyTo=true", actual);
		actual = c2t.getRouteName();
		assertEquals("TestOutboundWithNoNameRoute", actual);
	}
}
