The purpose of the esb-util
====================

This module contains base and utility classes for simplifying sending messages over the internal ESB and enforcing that this is done in a consistent way.

Using these classes enforces that all headers are set in a consistent way and that the same message format is used for all messages. 


Prerequisites
-----------------

The prerequisites for using the classes in this module is that Spring is used for dependency injection and that Camel routes are used for handling the communication.

The message format on this ESB is JSON using Jackson.


Usage
====================

A short overview on how to use the classes in this module.

In this example we want to send an event that a user has been modified using JSON message format over a JMS queue.


Add to Spring configure
------------------------

This example code was done for a Spring Boot project. 

First we need to add this dependency to the Application class, so the service is present in the Spring configuration:

	@Bean
	public OutboundMsgService outboundMsgService() {
        	return new OutboundMsgServiceImpl();
	}



Create Camel route
------------------

We create a Camel route which is used to send the message, this is annotated with the info needed to send the message to the correct channel.

	@OutboundEndpoint(channel="AHP.USER.MODIFIED.EVENT", 
                                channelType="jms", 
                                routeName="UserModifiedEventOutboundRoute", 
                                parameters={"jmsMessageType=Text", "disableReplyTo=true"})
	@Component
	public class UserModifiedEventOutboundRoute extends OutboundRoute {
	}


Send the message
----------------

Now we add the code to send the actual message.

This is done by:

* "injecting" an OutboundEndpoint class with the Camel route to the actual route we want to use (the route is "direct:" + the route name we set in the annotation to the route).
* Creating an OutboundMessage with the mandatory data
* Sending this msg using the injected OutboundEndpoint "object" 
An example of a class sending a msg:

	public class UserControllerImpl implements UserController {
	
		@Produce(uri="direct:UserModifiedEventOutboundRoute")
		OutboundEndpoint outboundEndpoint;
		
		private void sendUserModifiedEvent(User updatedUser, ModificationType modificationType) {
			UserModifiedEvent event = new UserModifiedEvent(UserConverter.convert(updatedUser), modificationType);
			OutboundMessage msg = new OutboundMessage(event, UUID.randomUUID().toString());
			outboundEndpoint.sendMsg(msg);
		}
		
	}

